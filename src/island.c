#include "island.h"
Island createIsland() {
	Island island;
	island.position = createVec3f(0.0f,0.0f,0.0f);
	island.health = 100.0f;
	island.platformRadius = 0.2f;

	//Cannon Stuff
	Vec3f cannonpos = createVec3f(0.0f,1.6f,0.0f);
	island.cannon = createCannon(cannonpos, 0.0f, 270.0f, 1.0f);
	return island;
}
//Draws the island pillar
void drawIsland(Island island) {
	glColor3f(0.275f, 0.510f, 0.706f);
	glPushMatrix();
		glTranslatef(0.0f,-1.0f,0.0f);
		glPushMatrix();
			glRotatef(-90,1,0,0);
			GLUquadricObj *p = gluNewQuadric();
			gluCylinder(p, island.platformRadius, island.platformRadius,1.6f,60,60);
			
			//Top of island
			glPushMatrix();
				glTranslatef(0,0,1.6f);
				glBegin(GL_POLYGON);
					for (int i=0; i <=360; i++){
						glVertex2f(cos(degToRad(i))*island.platformRadius, sin(degToRad(i))*island.platformRadius);
					}
				glEnd();
			glPopMatrix();

			//Bottom of island
			glPushMatrix();
				glBegin(GL_POLYGON);
					for (int i=0; i <=360; i++){
						glVertex2f(cos(degToRad(i))*island.platformRadius, sin(degToRad(i))*island.platformRadius);
					}
				glEnd();
			glPopMatrix();
		glPopMatrix();
		drawHousing(island.cannon);
	glPopMatrix();	
}