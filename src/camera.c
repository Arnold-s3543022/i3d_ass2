#include "camera.h"

//Create Camera
Camera createCamera() {
    Camera camera = {0};

    camera.position = createVec3f(0.0f, 0.0f, 0.0f);
    camera.pitch = 0.0f;
    camera.yaw = 0.0f;
    camera.roll = 0.0f;
    //Not sure if this will come in handy for later?
    //camera.rotation = createVec3f(0.0f, -90.0f, 0.0f);
    updateCamera(&camera);
    camera.fov = 60.0f;

    return camera;
}

//Update the camera look at
void updateCamera(Camera *camera) {
    camera->lookAt.x = cosf(degToRad(camera->yaw)) * cosf(degToRad(camera->pitch));
    camera->lookAt.y = sinf(degToRad(camera->pitch));
    camera->lookAt.z = sinf(degToRad(camera->yaw)) * cosf(degToRad(camera->pitch));
}

//Move the camera towards what we are looking at
void moveCamera(Camera *camera, Vec3f vel, float dt) {  
    camera->position.x += (camera->lookAt.x * vel.x) * dt;
    camera->position.y += (camera->lookAt.y * vel.y) * dt;
    camera->position.z += (camera->lookAt.z * vel.z) * dt;
}

//Set the camera position
void setCameraPos(Camera *camera, Vec3f pos) {
    camera->position.x = pos.x;
    camera->position.y = pos.y;
    camera->position.z = pos.z;
}

//This isn't working
void strafeCamera(Camera *camera, Vec3f vel, float dt) {
    camera->position.x += (camera->lookAt.x * vel.x) * dt;
}

//Rotate camera
void rotateCamera(Camera *camera, float x, float y, float dt) {
    if(camera->pitch >= 89.0f) {
        camera->pitch = 89.0f;
    } else if(camera->pitch <= -89.0f){
        camera->pitch = -89.0f;
    }

    camera->pitch += x * dt;

    if(camera->yaw >= 360.0f || camera->yaw <= -360.0f) {
        camera->yaw = 0.0;
    }

    camera->yaw += y * dt;
}