/* Terrain 
-   Island
    For now go with a yellow box in the middle

-   Water
    This would probably use a bunch of triangles? to simulate waves??

-   Sun?
    Light in the sky

-   Skybox?
    Surrounding box that has a sky
*/

#ifndef TERRAIN_H
#define TERRAIN_H

#include "platform.h"
#include "vector3.h"

typedef struct water {
    //Water
    //Hopefully we get to use these
    /*In terms we could save the vertices and apply 
    a different sine variable to different points to generate 
    weirder waves???*/
    // float *vertices;
    // float *texcoords;
    // float *indices;
    int segments;

    //Sine wave variables
    float amplitude;
    float wavelength;
    float timeperiod;
    float totalTime;

    //Debug Variables
    bool wireframe;
    bool animate;
    bool tangents;
    bool binormals;
    bool normals;
} Water;

typedef struct floor {
    int segments;
} Floor;

Water createWater();
void updateWater(Water *water, float dt);
void drawWater(Water water);
void drawDebug(Water water, float step);

float calcSine(Water water, float x);
float calcSineTangent(Water water, float x);

#endif