#include "terrain.h"

//Create the water
Water createWater() {
    Water water = {0};

    water.segments = 64;
    water.amplitude = 0.3f;
    water.wavelength = 2.3f;
    water.timeperiod = 0.5f;
    water.totalTime = 0.0f;
    water.wireframe = false;
    water.animate = true;
    water.tangents = false;
    water.binormals = false;
    water.normals = false;

    return water;
}

//Update water
void updateWater(Water *water, float dt) {
    if(water->animate) {
        water->totalTime += dt;
    }
}

//Draw water
void drawWater(Water water) {
    float c1[] = {0.0f, 0.2f, 1.0f, 0.5f};
    float c2[] = {0.0f, 0.6f, 1.0f, 0.5f};
    float step = 8.0 / water.segments;                
    Vec3f pos = {0.0f, 0.0f, 0.0f};

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if(water.wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    for (int j = 0; j < water.segments; j++) {

        if((j % 2) == 0) {
            glColor4f(0.0f, 0.2f, 1.0f, 0.5f);
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, c1);
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c1);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c1);
        } else {
            glColor4f(0.0f, 0.6f, 1.0f, 0.5f);
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, c2);
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c2);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c2);
        }

        glBegin(GL_TRIANGLE_STRIP);

        pos.z = -4.0 + j * step;
        float z2 = 0;

        for (int i = 0; i <= water.segments; i++) {
            pos.x = -4.0 + i * step;
            pos.y = calcSine(water, pos.x);

            z2 = -4.0 + (j + 1) * step;

            //Do normals make the lighting better or do surface normals make it better??
            //lets try normals here??
            float dy = calcSineTangent(water, pos.x);
            float dx = 1;

            glVertex3f(pos.x, pos.y, pos.z);
            glNormal3f(pos.x - dy, pos.y + dx, pos.z);
            glVertex3f(pos.x, pos.y, z2);
        }

        glEnd();

        CHECK_FOR_GL_ERROR;

        glBegin(GL_TRIANGLE_STRIP);

        pos.z = -4.0 + j * step;

        for (int i = 0; i <= water.segments; i++) {
            pos.x = -4.0 + i * step;
            pos.y = -1;

            z2 = -4.0 + (j + 1) * step;

            //Poop Brown
            float poop[] = {0.545, 0.271, 0.075};
            glColor3f(0.545, 0.271, 0.075);
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, poop);
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, poop);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, poop);
            glVertex3f(pos.x, pos.y, pos.z);
            glVertex3f(pos.x, pos.y, z2);
        }

        glEnd();
    }

    glDisable(GL_BLEND);

    //Draw the debug stuff, normals, tangents etc.
    drawDebug(water, step);
}

void drawDebug(Water water, float step) {

    Vec3f origin = createVec3f(0.0f, 0.0f, 0.0f);
    Vec3f dest = createVec3f(0.0f, 0.0f, 0.0f);

    for (int j = 0; j <= water.segments; j++) {

        origin.z = -4.0 + j * step;

        for(int i = 0; i <= water.segments; i++) {
            origin.x = -4.0 + i * step;
            origin.y = calcSine(water, origin.x);

            float dx = 1;
            float dy = calcSineTangent(water, origin.x);
            float dz = 1;

            float l = sqrtf(dx * dx + dy * dy + dz * dz);
            l /= 0.15;

            dx /= l;
            dy /= l;
            dz /= l;

            //Togle
            if(water.tangents) {
                dest.x = origin.x + dx;
                dest.y = origin.y + dy;
                dest.z = origin.z;

                drawVector(origin, dest, (Vec3f){1.0f, 0.0f, 0.0f});
            }
            
            //Toggle
            if(water.normals) {
                dest.x = origin.x - dy;
                dest.y = origin.y + dx;
                dest.z = origin.z;

                drawVector(origin, dest, (Vec3f){1.0f, 1.0f, 0.0f});
            }
            
            //Is this how you do binormals???
            if(water.binormals) {
                dest.x = origin.x;
                dest.y = origin.y + dy;
                dest.z = origin.z + dz;

                drawVector(origin, dest, (Vec3f){1.0f, 0.0f, 1.0f});
            }
        }
    }
}

//Calc Height 
float calcSine(Water water, float x) {
    //                                          this way you can pause animation
    // amp * sinf(wavelength * x + timeperiod * totalTime(dt += dt))
    float y = water.amplitude * sinf((water.wavelength * x) + (water.timeperiod * water.totalTime));
    return y;
}

//Calc derivate of the height func
float calcSineTangent(Water water, float x) {
    float dy = water.amplitude * water.wavelength * cosf((water.wavelength * x) + (water.timeperiod * water.totalTime));
    return dy;
}