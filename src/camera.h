/* Camera

*/
#ifndef CAMERA_H
#define CAMERA_H

#include "platform.h"
#include "vector3.h"

typedef struct camera {
    Vec3f position; //Position of camera
    Vec3f lookAt; //The "EYE" Angles
    float pitch, yaw, roll;
    float fov;
} Camera;

Camera createCamera();
void updateCamera(Camera *camera);
void moveCamera(Camera *camera, Vec3f vel, float dt);
void setCameraPos(Camera *camera, Vec3f pos);
void strafeCamera(Camera *camera, Vec3f vel, float dt);
void rotateCamera(Camera *camera, float x, float y, float dt);

#endif