#include "boat.h"

//Used blender to get these values LOL
float boatVertices[] = {
    1.0f, 0.0f, 1.0f,
    3.0f, 1.0f, 1.0f,
    1.0f, 0.0f, -1.0f,
    3.0f, 1.0f, -1.0f,
    -3.0f, 1.0f, 1.0f,
    -1.0f, 0.0f, 1.0f,
    -3.0f, 1.0f, -1.0f,
    -1.0f, 0.0f, -1.0f
};

//Used blender to get these values LOL
int boatIndices[]  = { 
    0, 1, 3, 2,
    4, 5, 7, 6,
    7, 2, 3, 6,
    5, 0, 1, 4,
    4, 1, 3, 6,
    5, 0, 2, 7
};

//Create the baot
Boat createBoat() {
    Boat boat = {0};

    boat.health = 10;
    //Just want to fix spawns so they are not close to island

    boat.pos = randomPos();
    boat.angle = 0.0f;

    boat.color = createColor3f(1.0f, 0.0f, 0.0f);
    boat.vertices = boatVertices;
    boat.indices = boatIndices;

    return boat;
}

//This should hold boat logic
//Firing, moving, etc.
void updateBoat(Boat *boat, Water water, float dt) {
    boat->health = 10;
    boat->pos.y = calcSine(water, boat->pos.x);
    if(boat->pos.x > 0.0f) {
        boat->pos.x -= 0.05f * dt;
    } else {
        boat->pos.x += 0.05f * dt;
    }

    if(boat->pos.z > 0.0f) {
        boat->pos.z -= 0.05f * dt;
    } else {
        boat->pos.z += 0.05f * dt;
    }
    
    boat->angle = radToDeg(atanf(calcSineTangent(water, boat->pos.x) / 1));


}

//Not sure why i did it this way
//Sometimes it cause I don't feel like having many glVertex calls :(
void renderBoat(Boat boat) {
    float work[] = {boat.color.x, boat.color.y, boat.color.z};

    glEnableClientState(GL_VERTEX_ARRAY);

    glPushMatrix();
        glTranslatef(boat.pos.x, boat.pos.y - 0.02f, boat.pos.z);
        glRotatef(boat.angle, 0, 0, 1);
        glScalef(0.05f, 0.05f, 0.05f);
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, work);
        glColor3f(boat.color.x, boat.color.y, boat.color.z);
        glVertexPointer(3, GL_FLOAT, 0, boat.vertices);
        glDrawElements(GL_QUADS, 24,  GL_UNSIGNED_INT, boat.indices);
        glColor3f(1.0f, 1.0f, 1.0f);
        glLineWidth(2.0f);
        glDrawElements(GL_LINES, 24,  GL_UNSIGNED_INT, boat.indices);
        glLineWidth(1.0f);
    glPopMatrix();

    glDisableClientState(GL_VERTEX_ARRAY);

    CHECK_FOR_GL_ERROR;
}

//Get a random position to spawn the baot
Vec3f randomPos() {
    float x, z;
    x = 8.0f * ((float)rand() / (float)RAND_MAX) - 4.0f;
    z = 8.0f * ((float)rand() / (float)RAND_MAX) - 4.0f;

    if(!(x > 3.0f) && !(x < -3.0f)) {
        int c1 = (rand() % (4 + 3) - 3);
        int c2 = (rand() % (4 + 3) - 3);
        x = (rand() > RAND_MAX/2) ? -c1 : c2;
    }

    if(!(z > 3.0f) && !(z < -3.0f)) {
        int c1 = (rand() % (4 + 3) - 3);
        int c2 = (rand() % (4 + 3) - 3);
        z = (rand() > RAND_MAX/2) ? -c1 : c2;
    }
    
    return (Vec3f){x, 0.0f, z};
}