/* Boat

- Position
    Should use collision detection for this?
    essentially let the boat sit on the water and only apply velocity and force
    in the other 2 directions.
- Rotation
    grab the normal from the terrain 
    and apply a rotation to it.
- Colour
- Cannon
    - Projectile
- Collision
    check against:
        water
        island
        cannonball
*/

#ifndef BOAT_H
#define BOAT_H

#include "platform.h"
#include "vector3.h"
#include "terrain.h"
#include "cannon.h"

typedef struct boat {
    int health;
    Vec3f pos;
    float angle;

    //Drawing Variables
    Color3f color;
    float *vertices;
    int *indices;

    //Cannon Variables
    Cannon cannon;
} Boat;

Boat createBoat();
void updateBoat(Boat *boat, Water water, float dt);
void renderBoat(Boat boat);

//Hopefully this is random enough
Vec3f randomPos();

#endif