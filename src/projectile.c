#include "projectile.h"

Projectile createProjectile(){
	Projectile projectile;
	projectile.position = createVec3f(0.0f, 50.0f, 0.0f);
	projectile.velocity = createVec3f(0.0f, 0.0f, 0.0f);
	projectile.size = 0.03f;
	projectile.speed = 4.0f;

	return projectile;
}
//Draws the projectile
void drawProjectile(Projectile projectile){
	glPushMatrix();
		glColor3f(0.275f, 0.510f, 0.706f);
		glTranslatef(projectile.position.x,projectile.position.y, projectile.position.z);
		glutSolidSphere(projectile.size,30,30);
	glPopMatrix();
}
//Moves projectile to the cannon
void initaliseProjectile(Projectile *projectile, float cannonLength, float pitch, Vec3f cannonPos, float rot) {
	//Moves projectile to tip of the cannon
	projectile->position.x = cannonLength * cosf(degToRad(-rot)) * cosf(degToRad(pitch)) + cannonPos.x;
	projectile->position.y = cannonLength * sin(degToRad(-pitch)) + cannonPos.y + 0.1f;
	projectile->position.z = cannonLength * sinf(degToRad(-rot)) * cosf(degToRad(pitch)) + cannonPos.z;
	//Velocity
	projectile->velocity.x = projectile->speed * cosf(degToRad(-rot)) * cosf(degToRad(pitch));
	projectile->velocity.y = projectile->speed * sin(degToRad(-pitch));
	projectile->velocity.z = projectile->speed * sinf(degToRad(-rot)) * cosf(degToRad(pitch));
	//printf("Velocity: %f,%f,%f\n",projectile->velocity.x,projectile->velocity.y,projectile->velocity.z);
}
//Fires the projectile 
void fireProjectile(Projectile *projectile, float dt, float g){
	//printf("DT:%f\n",dt);
	projectile->velocity.y += g*dt;
	projectile->position.x += projectile->velocity.x * dt;
	projectile->position.y += projectile->velocity.y * dt;
	projectile->position.z += projectile->velocity.z * dt;
	//printf("Fired Position: %f,%f,%f\n",projectile->position.x,projectile->position.y,projectile->position.z);
}