/* Cannon
*/

#ifndef CANNON_H
#define CANNON_H

#include "platform.h"
#include "vector3.h"
#include "projectile.h"

typedef struct cannon {
    Vec3f pos;
    float rot;
    float pitch;
    float scale;
    float length;
    Color3f color;
    Projectile projectile;
    //Projectile array 10? is a good amount im assuming
} Cannon;

Cannon createCannon(Vec3f pos, float rotation, float pitch, float scale);
void rotateCannon(Cannon *cannon, float rotation, float dt);
void pitchCannon(Cannon *cannon, float pitch, float dt);

void drawHousing(Cannon cannon);
void drawCannon(float pitch);

#endif