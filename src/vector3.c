#include "vector3.h"

//Self explanatory vector operations and functions

Vec3f createVec3f(float x, float y, float z) {
    Vec3f result;

    result.x = x;
    result.y = y;
    result.z = z;

    return result;
}

Vec3f addVec3f(Vec3f a, Vec3f b) {
    Vec3f result;

    result.x = a.x + b.x;
    result.y = a.y + b.y;
    result.z = a.z + b.z;

    return result;
}

Vec3f subtractVec3f(Vec3f a, Vec3f b) {
    Vec3f result;

    result.x = a.x - b.x;
    result.y = a.y - b.y;
    result.z = a.z - b.z;

    return result;
}

Vec3f multiplyVec3f(Vec3f a, Vec3f b) {
    Vec3f result;

    result.x = a.x * b.x;
    result.y = a.y * b.y;
    result.z = a.z * b.z;

    return result;
}

Vec3f divideVec3f(Vec3f a, Vec3f b) {
    Vec3f result;

    result.x = a.x / b.x;
    result.y = a.y / b.y;
    result.z = a.z / b.z;

    return result;
}

bool equalsVec3f(Vec3f a, Vec3f b) {
    bool result = (a.x == b.x && a.y == b.y && a.z == b.z);
    return result;
}

float dotVec3f(Vec3f a, Vec3f b) {
    float result = (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
    return result;
}

Vec3f crossVec3f(Vec3f a, Vec3f b) {
    Vec3f result;

    result.x = (a.y * b.z) - (a.z * b.y);
    result.y = (a.z * b.x) - (a.x * b.z);
    result.z = (a.x * b.y) - (a.y * b.x);

    return result;
}

float lengthVec3f(Vec3f a) {
    float result = sqrtf(dotVec3f(a, a));
    return result;
}

Vec3f normalizeVec3f(Vec3f a) {
    Vec3f result;
    float length = lengthVec3f(a);

    if(length != 0.0f) {
        result.x = a.x * (1.0f / length);
        result.y = a.y * (1.0f / length);
        result.z = a.z * (1.0f / length);
    }

    return result;
}

//Color stuff?
Color3f createColor3f(float r, float g, float b) {
    return createVec3f(r, g, b);
}

//OpenGL Stuff?
void drawVector(Vec3f a, Vec3f b, Vec3f col) {
    glBegin(GL_LINES);
    glColor3f(col.x, col.y, col.z);
    glVertex3f(a.x, a.y, a.z);
    glVertex3f(b.x, b.y, b.z);
    glEnd();
    CHECK_FOR_GL_ERROR;
}