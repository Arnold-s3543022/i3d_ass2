#include "platform.h"
#include "vector3.h"
#include "cannon.h"

typedef struct island{
	Vec3f position;
	float health;
	float platformRadius;

	Cannon cannon;
	Vec3f cannonPosition;
	float cannonRotation;
	float cannonPitch;
} Island;

Island createIsland();
void drawIsland();
