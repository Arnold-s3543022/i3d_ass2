/*
    The start of i3d_ass2
*/

/*
TODO:
    Terrain
    -   Water and Island (can easily have this in the same file terrain.h)

    Cannon
    -   Make this one class so we can reuse the same cannon functions
    
    Projectile
    -   Should this be inside of cannon? like a subclass?
    -   Each cannon can only shoot x amount?
    -   Types of projectiles? eg. glutCone? glutSphere
        - Use glut in built 3d models
        - Make us shoot a teapot ;D

    Boat
    -   Drawing the boat
    -   Creating the boat
    -   This will contain some logic for moving the boat etc.
    -   Utilize Cannon

    Camera
    -   Thought of having 3 types of modes for this camera.
        1 - We control it, kinda like a "noclip" mode - Done
        2 - We stick it onto the cannon "3rd person" - This hard
        3 - We stick it in the cannon "1st person" - stuff this option stick to the top 2

COMPLETED:
    Vec3

Questions:
    DO we need a player class if he is just the cannon/island?
    could create a struct here with a hp value etc...?
*/

#include "platform.h"
#include "terrain.h"
#include "boat.h"
#include "camera.h"
#include "island.h"
#include "cannon.h"

typedef struct game {
    //Timing
    float startTime;
	int frames;
	float frameRate;
	float frameRateInterval;
	float lastFrameRateT;
    float dt;

    //Mouse
    float mx;
    float my;

    //Debug
    bool debug;
    bool wireframe;
    bool lighting;
    bool islandFire;
} Game;

//Its fine to have global variables ;)
Game game;
Boat boats[10];
Camera camera;
Water water;
Island island;

//Are these correct?
Vec3f forward = {15.0f, 15.0f, 15.0f};
Vec3f backward = {-15.0f, -15.0f, -15.0f};

bool keyStates[256];

//This acts as a key press and release
void keyInput() {
    if(keyStates[27]) { //Escape
        exit(EXIT_SUCCESS);
    }
    
    if(game.debug) {
        //Move the camera Position
        if(keyStates['w']) {
            moveCamera(&camera, forward, game.dt);
        }
        if(keyStates['s']) {
            moveCamera(&camera, backward, game.dt);
        }
        
        //Rotate the camera to affect the looking angles
        if(keyStates['j']) {
            rotateCamera(&camera, 0, -45.0f, game.dt);
        }
        if(keyStates['l']) {
            rotateCamera(&camera, 0, 45.0f, game.dt);
        }
        if(keyStates['i']) {
            rotateCamera(&camera, 45.0f, 0, game.dt);
        }
        if(keyStates['k']) {
            rotateCamera(&camera, -45.0f, 0, game.dt);
        }
    }

    if(keyStates['f']) {
        rotateCannon(&island.cannon, 20.0f, game.dt);
    }
    if(keyStates['h']) {
        rotateCannon(&island.cannon, -20.0f, game.dt);
    }

    if(keyStates['t']) {
        if(island.cannon.pitch > 270.0f) {
            pitchCannon(&island.cannon, -20.0f, game.dt);
        } 
    }
    if(keyStates['g']) {
        if(island.cannon.pitch < 360.0f) {
            pitchCannon(&island.cannon, 20.0f, game.dt);
        }
    }
}

void keyPressed(unsigned char key, int x, int y) {  
    keyStates[key] = true; // Set the state of the current key to pressed  

    switch(key) {

        case 'm':
            water.tangents = !water.tangents;
        break;
        case 'n':
            water.normals = !water.normals;
        break;

        //Not sure if binormals are calculated correctly
        case 'b':
            water.binormals = !water.binormals;
        break;

        case '`':
            game.debug = !game.debug;
        break;
        case '1':
            game.wireframe = !game.wireframe;
            water.wireframe = !water.wireframe;
        break;
        case '2':
            game.lighting = !game.lighting;
        break;
        case ' ':
            game.islandFire = !game.islandFire;
            initaliseProjectile(&island.cannon.projectile,
            island.cannon.length,
            island.cannon.pitch,
            island.cannon.pos,
            island.cannon.rot);
        break;
        default:
        break;
    }
}  
  
void keyReleased(unsigned char key, int x, int y) {  
    keyStates[key] = false; // Set the state of the current key to not pressed  
} 

void mousePassiveMove(int x, int y) {
    // x = ((float)x - (float)(640 / 2));
    // y = ((float)y - (float)(480 / 2));
    
    // //printf("Mouse X = %f, Mouse Y = %f\n", game.mx, mdy);

    // float deltaX = x - game.mx;
    // float deltaY = y - game.my;

    // rotateCamera(&camera, deltaX, deltaY, game.dt);
}

void displayOSD() {
    char buffer[30];
	char *bufp;
	int w, h;

	glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	/* Set up orthographic coordinate system to match the
	   window, i.e. (0,0)-(w,h) */
	w = glutGet(GLUT_WINDOW_WIDTH);
	h = glutGet(GLUT_WINDOW_HEIGHT);
	glOrtho(0.0, w, 0.0, h, -1.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	/* Frame rate */
	glColor3f(1.0, 1.0, 1.0);
	glRasterPos2i(w - 150, h - 15);
	snprintf(buffer, sizeof buffer, "fr (f/s): %6.0f", game.frameRate);
	for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	/* Time per frame */
	glColor3f(1.0, 1.0, 1.0);
	glRasterPos2i(w - 150, h - 30);
	snprintf(buffer, sizeof buffer, "ft (ms/f): %5.0f", 1.0 / game.frameRate * 1000.0);
	for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	glColor3f(1.0, 1.0, 1.0);
	glRasterPos2i(w - w, h - 15);
	snprintf(buffer, sizeof buffer, "mx: %4.0f | my: %.0f", game.mx, game.my);
	for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    glColor3f(1.0, 1.0, 1.0);
	glRasterPos2i(w - w, h - 30);
	snprintf(buffer, sizeof buffer, "cam.yaw: %f", camera.yaw);
	for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    glColor3f(1.0, 1.0, 1.0);
	glRasterPos2i(w - w, h - 45);
	snprintf(buffer, sizeof buffer, "cam.pitch: %f", camera.pitch);
	for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    glColor3f(1.0, 1.0, 1.0);
	glRasterPos2i(w - w, h - 60);
	snprintf(buffer, sizeof buffer, "cam.x: %f", camera.position.x);
	for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    glColor3f(1.0, 1.0, 1.0);
	glRasterPos2i(w - w, h - 75);
	snprintf(buffer, sizeof buffer, "cam.y: %f", camera.position.y);
	for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    glColor3f(1.0, 1.0, 1.0);
	glRasterPos2i(w - w, h - 90);
	snprintf(buffer, sizeof buffer, "cam.z: %f", camera.position.z);
	for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	/* Pop modelview */
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);

	/* Pop projection */
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	/* Pop attributes */
	glPopAttrib();
}

GLfloat light_ambient[] = {1.0, 1.0, 1.0, 1.0};
GLfloat light_position[] = {0.0, -1.0, 0.0, 0.0};

void displayGame(void) {
    // glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseMaterial);
    // glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
    // glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 25.0);
    
    if(game.lighting) {
        glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
        // glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_ambient);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
    }

    glShadeModel(GL_SMOOTH);
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    //Origin Axis
    if(game.debug) {
        glBegin(GL_LINES);
            // glMaterial
            glColor3f(1.0f, 0.0f, 0.0f);
            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(1.0f, 0.0f, 0.0f); 

            glColor3f(0.0f, 1.0f, 0.0f);
            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(0.0f, 1.0f, 0.0f);

            glColor3f(0.0f, 0.0f, 1.0f);
            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(0.0f, 0.0f, 1.0f);
        glEnd();

        gluLookAt(
    //Camera Position
        camera.position.x, 
        camera.position.y, 
        camera.position.z, 
    //Camera Direction
        camera.position.x + camera.lookAt.x, 
        camera.position.y + camera.lookAt.y, 
        camera.position.z + camera.lookAt.z, 
    //Camera Up Vector
        0.0f, 1.0f, 0.0f);
    } else {
        float x = cosf(degToRad(-island.cannon.rot)) * cosf(degToRad(-island.cannon.pitch));
        float y = sinf(degToRad(-island.cannon.pitch));
        float z = sinf(degToRad(-island.cannon.rot)) * cosf(degToRad(-island.cannon.pitch));
        gluLookAt(
    //Camera Position
        island.position.x, 
        island.position.y +0.9, 
        island.position.z, 
    //Camera Direction
        island.position.x + x, 
        island.position.y + y, 
        island.position.z + z, 
    //Camera Up Vector
        0.0f, 1.0f, 0.0f);
    }

    

    if(game.wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    // glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colorRed);
    for(int i = 0; i < (int)COUNT_OF(boats); i++) {
        renderBoat(boats[i]);
    }

    // glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colorBlue);
    drawWater(water);

    glDisable(GL_LIGHTING);

    drawIsland(island);

    displayOSD();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


    game.frames++;

    glutSwapBuffers();
}

void updateGame(void) {
    static float lastT = -1.0;
	float t, dt;
	int milli = 1000;
	//gravity
	float g = -9.8;

	t = glutGet(GLUT_ELAPSED_TIME) / (float)milli - game.startTime;

	if (lastT < 0.0) {
		lastT = t;
		return;
	}

	game.dt = t - lastT;

	lastT = t;

    /* Handle Key Input */ 
    keyInput();

    updateCamera(&camera);
    if(game.islandFire){
        fireProjectile(&island.cannon.projectile,game.dt,g);
    }
    updateWater(&water, game.dt);
    for(int i = 0; i < (int)COUNT_OF(boats); i++) {
        updateBoat(&boats[i], water, game.dt);
    }
    

    /* Frame rate */
	game.dt = t - game.lastFrameRateT;
	if (game.dt > game.frameRateInterval) {
		game.frameRate = game.frames / game.dt;
		game.lastFrameRateT = t;
		game.frames = 0;
	}

	glutPostRedisplay();
}

void resizeGame(int w, int h) {
    glViewport(0, 0, (GLsizei)w, (GLsizei)h); 
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float)(w/h), 0.01f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char** argv) {
    printf("Hello, World!\n");

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Island Defence - 3D");

    //Main Callbacks
    glutDisplayFunc(displayGame);
    glutReshapeFunc(resizeGame);
    glutIdleFunc(updateGame);
    glutKeyboardFunc(keyPressed);
    glutKeyboardUpFunc(keyReleased);
    glutPassiveMotionFunc(mousePassiveMove);

    game.startTime = 0.0f;
	game.frames = 0;
	game.frameRate = 0.0f;
	game.frameRateInterval = 0.0f;
	game.lastFrameRateT = 0.0f;
    game.mx = 0;
    game.my = 0;
    game.debug = true;
    game.islandFire = false;

    for(int i = 0; i < (int)COUNT_OF(boats); i++) {
        boats[i] = createBoat();
    }

    camera = createCamera();
    setCameraPos(&camera, (Vec3f){-2.0f, 3.0f, 3.0f});
    rotateCamera(&camera, -25.0f, -51.0f, 1.0f);

    water = createWater();
    island = createIsland();

    //Main Loop
    glutMainLoop();

    return 0;
}