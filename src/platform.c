#include "platform.h"

int checkForGLError(const char* file, int line, const char* func) {
	GLenum err = glGetError();

	if (err != GL_NO_ERROR) {
		fprintf(stderr, "GL error on line %d in file %s in function %s: ", line, file, func);

		switch (err) {
		case GL_INVALID_ENUM:
			fprintf(stderr, "invalid enum");
			break;
		case GL_INVALID_VALUE:
			fprintf(stderr, "invalid value");
			break;
		case GL_INVALID_OPERATION:
			fprintf(stderr, "invalid operation");
			break;
		// case GL_INVALID_FRAMEBUFFER_OPERATION:
		// 	fprintf(stderr, "invalid framebuffer operation");
		// 	break;
		case GL_OUT_OF_MEMORY:
			fprintf(stderr, "out of memory");
			break;
		case GL_STACK_UNDERFLOW:
			fprintf(stderr, "stack underflow");
			break;
		case GL_STACK_OVERFLOW:
			fprintf(stderr, "stack overflow");
			break;
		default:
			fprintf(stderr, "unknown error");
			break;
		}

		fprintf(stderr, "\n");
		return 1;
	}

	return 0;
}