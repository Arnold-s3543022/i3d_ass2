=========== STUDENTS ===========
Student 1 - Mark Nguyen s3539688
Student 2 - Arnold D'Silva s3543022

=========== CONTROLS ===========

=========== Camera Control ===========
W = Translate Camera Forward
S = Translate Camera Back

I = Pitch Camera Up
K = Pitch Camera Down
J = Rotate Camera Left
L = Rotate Camera Right

=========== Cannon Controls ===========
T = Pitch Cannon Up
G = Pitch Cannon Down
F = Rotate Cannon Right
H = Rotate Cannon Left
SPACEBAR = Fires Cannonball (You have to press twice to fire again)

=========== Debugging Controls ===========
M = Toggles Tangents
N = Toggles Normals
B = Toggles Bi-Normals
` = Toggles First Person Camera
1 = Toggles  wireframes
2 = Toggles /Lighting
