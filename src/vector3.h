#ifndef VECTOR3_H
#define VECTOR3_H

#include "platform.h"

typedef struct vec3 {
    float x, y, z;
} Vec3f, Color3f;

Vec3f createVec3f(float x, float y, float z);
Vec3f addVec3f(Vec3f a, Vec3f b);
Vec3f subtractVec3f(Vec3f a, Vec3f b);
Vec3f multiplyVec3f(Vec3f a, Vec3f b);
Vec3f divideVec3f(Vec3f a, Vec3f b);
bool equalsVec3f(Vec3f a, Vec3f b);
float dotVec3f(Vec3f a, Vec3f b);
Vec3f crossVec3f(Vec3f a, Vec3f b);
float lengthVec3f(Vec3f a);
Vec3f normalizeVec3f(Vec3f a);

Color3f createColor3f(float r, float g, float b);

void drawVector(Vec3f a, Vec3f b, Vec3f col);

#endif