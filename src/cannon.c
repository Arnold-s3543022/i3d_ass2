#include "cannon.h"

Cannon createCannon(Vec3f pos, float rotation, float pitch, float scale) {
	Cannon cannon;
	cannon.pos = pos;
	cannon.rot = rotation;
	cannon.pitch = pitch;
	cannon.scale = scale;
	cannon.length = 0.25f;
	cannon.projectile = createProjectile();	
	return cannon;
}
//Changes the rotation value of the current cannon
void rotateCannon(Cannon *cannon, float rotation, float dt) {
	cannon->rot += rotation * dt;
}
//Changes the pitch value of the cannon barrel
void pitchCannon(Cannon *cannon, float pitch, float dt) {
	cannon->pitch += pitch * dt;
}
//Draws the housing of the cannon
void drawHousing(Cannon cannon) {

	glPushMatrix();
		glColor3f(0.827f, 0.827f, 0.827f);//Dark grey
		glTranslatef(cannon.pos.x, cannon.pos.y, cannon.pos.z);
		glRotatef(cannon.rot, 0.0f, 1.0f, 0.0f);
		glScalef(cannon.scale, cannon.scale, cannon.scale);
		//Base of platform
		glutSolidCube(0.2f);
		//Round Part of Platform
		glPushMatrix();
			//Cylinder
			glTranslatef(0.0f,0.1f,-0.098f);
			GLUquadricObj *p = gluNewQuadric();
			gluCylinder(p, 0.1f, 0.1f,0.196f,60,60);

			//Bottom of cylinder
			glBegin(GL_POLYGON);
				for (int i=0; i <=360; i++){
					glVertex2f(cos(degToRad(i))*0.1f, sin(degToRad(i))*0.1f);
				}
			glEnd();

			//Top of Cylinder
			glPushMatrix();
				glTranslatef(0,0,0.196f);

				glBegin(GL_POLYGON);
					for (int i=0; i <=360; i++){
						glVertex2f(cos(degToRad(i))*0.1f, sin(degToRad(i))*0.1f);
					}
				glEnd();

			glPopMatrix();
			drawCannon(cannon.pitch);
		glPopMatrix();
	glPopMatrix();
	drawProjectile(cannon.projectile);
}
//Draws the barrel of the cannon
void drawCannon(float pitch) {
	glPushMatrix();
		glTranslatef(0.0f,0.0f,0.098f);
		glColor3f(0.275f, 0.510f, 0.706f);
		glRotatef(90.0f,0.0f,1.0f,0.0f);
		glRotatef(pitch,1.0f,0.0f,0.0f);
		GLUquadricObj *p = gluNewQuadric();
		gluCylinder(p, 0.03f, 0.03f,0.25f,60,60);

		glPushMatrix();
			glColor3f(0.1f, 0.1f, 0.1f);//Dark grey
			glTranslatef(0,0,0.25);
			glBegin(GL_POLYGON);
				for (int i=0; i <=360; i++){
					glVertex2f(cos(degToRad(i))*0.03f, sin(degToRad(i))*0.03f);
				}
			glEnd();
		glPopMatrix();
	glPopMatrix();
}