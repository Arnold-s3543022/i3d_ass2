/* Projectile
*/
#include "platform.h"
#include "vector3.h"

typedef struct projectile {
    Vec3f position;
    Vec3f velocity;
    float size;
    float speed;
} Projectile;

Projectile createProjectile();
void drawProjectile(Projectile projectile);
void initaliseProjectile(Projectile *projectile,float cannonLength, float pitch, Vec3f cannonPos, float rot);
void fireProjectile(Projectile *projectile, float dt, float g);