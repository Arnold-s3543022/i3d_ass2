# README

pull the code

compile the code

run the game

# Marking Guide

This is a guide only, and may change.

## PA: 
basic assignment with boats bobbing and rocking on 3D water and and 3D island with lighting and scene viewer/camera

## CR: 
boats spawning with simple AI directing them towards the island, firing cannonballs with projectile motion as well as an island with a cannon able to similarly fire a cannon.

## DI: 
projectile path display, collision detection, texturing

## HD: 
polished gameplay, multiple missiles, cooldown, rendering, OSD, missle defence, complex water, better boat AI, use of arrays to store vertex data

# Note: Quality of implementation, including code, and fine detail matter and will be taken into account e.g. correctly firing cannon balls from end of turret rather than centre of boat

# Last modified April 25, 2018 at 10:05 